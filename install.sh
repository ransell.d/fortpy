#cp fortran_codes/* .
gfortran -c fort_py.f95 
gfortran fort_py.o linspace_ex.f95 -o linspace.x
gfortran fort_py.o interp1d_ex.f95 -o interp1d.x
gfortran fort_py.o trapz_ex.f95 -o trapz.x
gfortran fort_py.o trapz_ex_unknown_func.f95 -o trapz_unknown_func.x
gfortran fort_py.o test_griddata.f95 -o grid2d.x
