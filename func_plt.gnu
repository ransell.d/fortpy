set title "Using linspace, interp1d, trapz in FortPy "
set xlabel "x"
set ylabel "f(x) = x**2"
set mxtics 5
set mytics 5
set title font "sans,15"
set xlabel font "sans,15"
set ylabel font "sans,15"
set tics font "sans,15"
set key font "sans,15"
set xrange [0:10]
#set yrange [0:7]
p 'square.dat' pt 7 ps 2, 'square_interp1d.dat' w l lw 3
pause -1 "press any key to continue"
