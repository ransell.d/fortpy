# FortPy

## Description
The aim of this project is to collect subroutines used commonly in scienctific computing and name them as they're named in python.
See examples of linspace, interpolation and integrating an analytical function, integrate a numerical function saved on a file, differentiating a function,  below. 
I've attempted to make a loadtxt file which reads data like np.loadtxt() and then use it to intepolate a 2D function from file.

## Install
```bash
sh install.sh
```

## Use
In python (numpy as np), if you wanted to use linspace, you'd use the following
```python
res = np.linspace(0, 10, 50)
```
The same can be done with FortPy using 
```fortran
call linspace(res, 0.0, 10.0 , 50)
```
similary for interp1d,
```fortran
call linspace(x_interp_array, 0.0, 10.0 , 1000)
call interp1d(x, y, n, x_interp_array(i), y_interp)
```

```bash
./linspace.x
./interp1d.x
./trapz.x
./trapz_unknown_func.x
./grid2d.x
```

## Results
- Here the purple points show f(x)=x$^2$ using linspace for 50 points.
- The green line is using interp1d for 1000 points.
- The trapz code integrates the analytical function from 0 to 1.
- The trapz_unknown_func integrates the numerical data (green curve) of interp1d from 2 to 6
![1d_results](./func_plot.png)

Doing basic calculus also becomes easier. For example
```bash
./int_trig.x > int_cos.dat 
./grad_trig.x > grad_cos.dat
gnuplot calculas_trig_plt.gnu 
```
The above examples will integrate and differentiate a trig function cos(x) and plot them.
![1d_calculus](./calculus_cos.png)

## Objectives
1. An attempt to rewrite one's python code to fortran with ease to gain speeds of orders of magnitude for scientific computing.

## Feedback
Send feedback to ransell.dsouza@utu.fi/ ransell.d@gmail.com
