module functions
use fort_py
implicit none
integer, parameter :: n = 1000
real, dimension(n) :: x, y
integer :: i
logical :: data_init = .false.

contains
subroutine get_data()
if (.not.data_init) then
open(10, file="square_interp1d.dat", status='old', action='read')
! Read the data into the arrays
do i = 1, n
    read(10, *) x(i), y(i)
end do
close(10)
data_init = .true.
end if
end subroutine get_data

real function f(xu)
    real, intent(in) :: xu
    real :: y_interp
    call get_data()
    call interp1d(x, y, n, xu, y_interp)
    f = y_interp
end function f

end module functions

program trapz_ex
use functions
implicit none
real :: xmin, xmax
integer, parameter :: num = 100000
real :: result

xmin = 2.0
xmax = 6.0
call trapz(f, xmin, xmax, num, result)
write(*,*) result
write(*,*) "Expected result: 69.33333333"
end program trapz_ex

