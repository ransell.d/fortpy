module fort_py
implicit none 
contains      

subroutine linspace(x, x_start, x_end, x_len)
real, dimension(:), intent(out) :: x
real :: x_start, x_end, dx
integer :: x_len, i

dx = (x_end - x_start)/(x_len - 1)
x(1:x_len) = [(x_start+((i-1)*dx),i=1,x_len)]
end subroutine linspace

subroutine trapz( f, xmin, xmax, steps, result )

interface
real function f( x )
real, intent(in) :: x
end function f
end interface

real, intent(in)    :: xmin, xmax
integer, intent(in) :: steps
real, intent(out)   :: result

integer             :: i
real                :: x
real                :: deltx

if ( steps <= 0 ) then
    result = 0.0
    return
endif

deltx = (xmax - xmin) / steps

result = ( f(xmin) + f(xmax) )/ 2.0

do i = 2,steps
    x      = xmin + (i - 1) * deltx
    result = result + f(x)
enddo

result = result * deltx
end subroutine trapz

subroutine interp1d(x, y, n, x_interp, y_interp)
implicit none
real, dimension(:), intent(in) :: x, y
integer, intent(in) :: n
real, intent(in) :: x_interp
real, intent(out) :: y_interp
integer :: i

! Initialize y_interp to a default value
y_interp = 0.0

! Check if x_interp is out of bounds
if (x_interp < x(1) .or. x_interp > x(n)) then
    print *, 'Error: x value out of bounds.'
    return
end if

! Perform linear interpolation
do i = 1, n-1
    if (x_interp >= x(i) .and. x_interp <= x(i+1)) then
        y_interp = y(i) + (y(i+1) - y(i)) * (x_interp - x(i)) / (x(i+1) - x(i))
        return
    end if
end do
end subroutine interp1d

subroutine gradient(f, x, n, df)
implicit none
interface
    real function f(x)
        real, intent(in) :: x
    end function f
end interface
integer, intent(in) :: n
real, dimension(n), intent(in) :: x
real, dimension(n), intent(out) :: df
integer :: i
real :: h

! Compute the derivative using central difference method
do i = 2, n-1
    h = x(i+1) - x(i-1)
    df(i) = (f(x(i+1)) - f(x(i-1))) / h
end do

! Forward difference for the first point
h = x(2) - x(1)
df(1) = (f(x(2)) - f(x(1))) / h

! Backward difference for the last point
h = x(n) - x(n-1)
df(n) = (f(x(n)) - f(x(n-1))) / h
end subroutine gradient

  ! 2D Linear interpolation using bilinear interpolation
subroutine griddata_2d(x, y, z, nx, ny, xi, yi, zi)
  implicit none

  integer, intent(in) :: nx, ny    ! Number of grid points in x and y
  real, intent(in) :: x(nx), y(ny)  ! Grid points in x and y directions
  real, intent(in) :: z(nx, ny)     ! Values at grid points (2D array)
  real, intent(in) :: xi, yi        ! Point where we want to interpolate
  real, intent(out) :: zi           ! Interpolated value

  integer :: i, j
  real :: x1, x2, y1, y2
  real :: z11, z12, z21, z22
  real :: denom, wx1, wx2, wy1, wy2

  ! Locate the indices i and j such that x(i) <= xi < x(i+1) and y(j) <= yi < y(j+1)
  call find_nearest_neighbors(x, nx, xi, i)
  call find_nearest_neighbors(y, ny, yi, j)

  ! Get the corner points of the rectangle surrounding (xi, yi)
  x1 = x(i)
  x2 = x(i+1)
  y1 = y(j)
  y2 = y(j+1)

  ! Get the values at the corners
  z11 = z(i, j)      ! z(x1, y1)
  z12 = z(i, j+1)    ! z(x1, y2)
  z21 = z(i+1, j)    ! z(x2, y1)
  z22 = z(i+1, j+1)  ! z(x2, y2)

  ! Calculate weights for interpolation
  denom = (x2 - x1) * (y2 - y1)
  wx1 = (x2 - xi) / (x2 - x1)
  wx2 = (xi - x1) / (x2 - x1)
  wy1 = (y2 - yi) / (y2 - y1)
  wy2 = (yi - y1) / (y2 - y1)

  ! Perform bilinear interpolation
  zi = (z11 * wx1 * wy1 + z21 * wx2 * wy1 + z12 * wx1 * wy2 + z22 * wx2 * wy2) !/ denom

end subroutine griddata_2d

subroutine find_nearest_neighbors(arr, n, target, idx)
  implicit none

  integer, intent(in) :: n        ! Number of elements in array
  real, intent(in) :: arr(n)   ! Array of values (e.g., x or y coordinates)
  real, intent(in) :: target   ! Target value to find neighbors for
  integer, intent(out) :: idx     ! Index of the lower neighbor

  integer :: i

  ! Find the largest index i such that arr(i) <= target
  do i = 1, n-1
    if (arr(i) <= target .and. target < arr(i+1)) then
      idx = i
      return
    end if
  end do

  ! If we reach here, target is outside the range of the array
  ! Handle this appropriately (e.g., clamp to boundary)
  if (target <= arr(1)) then
    idx = 1
  else
    idx = n - 1
  end if
end subroutine find_nearest_neighbors

subroutine loadtxt3d(filename, n_tot, x_, y_, z)
  implicit none
  integer :: n_tot
  real, dimension(:), intent(out) :: x_, y_, z
  integer :: i
  character(len=*), intent(in) :: filename  ! Accept the filename as input

  ! Open the file using the filename passed as an argument
  open(10, file=filename, status='old', action='read')

  ! Read the data into the arrays
  do i = 1, n_tot
    read(10, *) x_(i), y_(i), z(i)
  end do

  ! Close the file
  close(10)

end subroutine loadtxt3d


end module fort_py
