module functions
    implicit none
    contains
    real function f( x )
    real, intent(in) :: x
    f = cos(x)
    end function f
end module functions

program trapz_ex
use fort_py
use functions
implicit none
real :: xmin, xmax
integer, parameter :: num = 10000, n = 100
real, parameter :: pi = 3.141592653589
real :: result
integer :: i
real, dimension(n) :: theta

xmin = 0

call linspace(theta, 0.0, 2*pi , n)
do i = 1, n
xmax = theta(i)
call trapz(f,xmin,xmax,num,result)
write(*,*) theta(i), result, sin(theta(i))
enddo
end program trapz_ex

