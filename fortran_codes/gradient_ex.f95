module functions
    implicit none
    contains
    real function f( x )
    real, intent(in) :: x
    f = cos(x)
    end function f
end module functions

program grad_ex
use fort_py
use functions
implicit none
integer, parameter :: num = 1000
integer :: i
real :: xmin, xmax
real, parameter :: h = 0.01, pi = 3.14159265358
real, dimension(num) :: df, theta, func, result

call linspace(theta, 0.0, 2*pi , num)

call gradient(f,theta,num, result)
do i = 1,num
write(*,*) theta(i), result(i)
enddo

end program grad_ex

