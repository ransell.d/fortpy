module functions
    implicit none
    contains
    real function f( x )
    real, intent(in) :: x
    f = x**2
    end function f
end module functions

program trapz_ex
use fort_py
use functions
implicit none
real :: xmin, xmax
integer, parameter :: num = 10000, pi = 3.141592653589
real :: result

xmin = 0
xmax = 1
call trapz(f,xmin,xmax,num,result)
write(*,*) result
write(*,*) "Expected answer 1/3"
end program trapz_ex

