program linspace_ex
use fort_py
implicit none

real :: strt, stp
integer, parameter :: num = 50
integer :: i
real, dimension(num) :: res

call linspace(res, 0.0, 10.0 , num)
do i = 1, num
write(*,*) res(i) !, res(i)**2
enddo

end program linspace_ex
