program test_interpolation
  use fort_py
  implicit none
  integer, parameter :: n_tot = 441, n = 21
  !integer, parameter :: n = 441
  real, dimension(n_tot) :: x_, y_, z
  real, dimension(n) :: x, y
  real, dimension(n, n) :: z_2d
  real :: xi, yi, zi, zi_expected
  integer :: i, j, c
  character(len=20) :: filename

  filename = "sin_x_y.dat"  

  ! Place data on grid
  call loadtxt3d(filename, n_tot, x_, y_, z)
  c = 1
  do i = 1, n
     do j = 1, n
        z_2d(i, j) = z(c)
        c = c + 1
     end do
  end do

  ! Point to interpolate
  xi = -1.00
  yi = -0.10
  zi_expected = -0.89121

  ! Call the interpolation subroutine
  call linspace(x, -1.00, 1.00 , n)
  call linspace(y, -1.00, 1.00 , n)
  call griddata_2d(x, y, z_2d, n, n, xi, yi, zi)

  print *, 'Interpolated value at (', xi, ', ', yi, ') is: ', zi
  print *, "Expected result:", zi_expected
end program test_interpolation

