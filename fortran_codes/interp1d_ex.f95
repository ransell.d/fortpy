program interp1d_ex
use fort_py

implicit none
real, dimension(:), allocatable :: x, y
real :: x_interp, y_interp
integer :: n, i, n_tot
real, allocatable :: x_interp_array(:) 

n = 50
n_tot = 1000
! Allocate the arrays
allocate(x(n), y(n))
allocate(x_interp_array(n_tot))

open(10, file="square.dat", status='old', action='read')
! Read the data into the arrays
do i = 1, n
    read(10, *) x(i), y(i)
end do

call linspace(x_interp_array, 0.0, 10.0 , n_tot)
do i = 1, n_tot
    call interp1d(x, y, n, x_interp_array(i), y_interp)
    write(*, *) x_interp_array(i), y_interp
end do

deallocate(x_interp_array,x,y)

end program interp1d_ex
